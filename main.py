from flask import Flask, render_template, url_for

app = Flask(__name__)

#global variable that can be used in our program
title = ''

#define my first route with flask
@app.route('/')
def home_page():
    title='test title'
    return render_template('index.html', title=title)


if __name__ == '__main__':
    app.run(debug=True)
